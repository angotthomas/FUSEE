gdjs.S15Code = {};
gdjs.S15Code.GDNewObjectObjects1= [];
gdjs.S15Code.GDNewObjectObjects2= [];
gdjs.S15Code.GDNewObject2Objects1= [];
gdjs.S15Code.GDNewObject2Objects2= [];

gdjs.S15Code.conditionTrue_0 = {val:false};
gdjs.S15Code.condition0IsTrue_0 = {val:false};
gdjs.S15Code.condition1IsTrue_0 = {val:false};
gdjs.S15Code.condition2IsTrue_0 = {val:false};


gdjs.S15Code.mapOfGDgdjs_46S15Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S15Code.GDNewObject2Objects1});gdjs.S15Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S15Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S15Code.condition0IsTrue_0.val = false;
gdjs.S15Code.condition1IsTrue_0.val = false;
{
gdjs.S15Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S15Code.condition0IsTrue_0.val ) {
{
gdjs.S15Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S15Code.mapOfGDgdjs_46S15Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S15Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "INTRO", true);
}}

}


}; //End of gdjs.S15Code.eventsList0xb25a8


gdjs.S15Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S15Code.GDNewObjectObjects1.length = 0;
gdjs.S15Code.GDNewObjectObjects2.length = 0;
gdjs.S15Code.GDNewObject2Objects1.length = 0;
gdjs.S15Code.GDNewObject2Objects2.length = 0;

gdjs.S15Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S15Code'] = gdjs.S15Code;
