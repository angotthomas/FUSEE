gdjs.S6Code = {};
gdjs.S6Code.GDNewObjectObjects1= [];
gdjs.S6Code.GDNewObjectObjects2= [];
gdjs.S6Code.GDNewObject2Objects1= [];
gdjs.S6Code.GDNewObject2Objects2= [];
gdjs.S6Code.GDNewObject3Objects1= [];
gdjs.S6Code.GDNewObject3Objects2= [];

gdjs.S6Code.conditionTrue_0 = {val:false};
gdjs.S6Code.condition0IsTrue_0 = {val:false};
gdjs.S6Code.condition1IsTrue_0 = {val:false};
gdjs.S6Code.condition2IsTrue_0 = {val:false};


gdjs.S6Code.mapOfGDgdjs_46S6Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S6Code.GDNewObject2Objects1});gdjs.S6Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S6Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S6Code.condition0IsTrue_0.val = false;
gdjs.S6Code.condition1IsTrue_0.val = false;
{
gdjs.S6Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S6Code.condition0IsTrue_0.val ) {
{
gdjs.S6Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S6Code.mapOfGDgdjs_46S6Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S6Code.condition1IsTrue_0.val) {
gdjs.S6Code.GDNewObject3Objects1.createFrom(runtimeScene.getObjects("NewObject3"));
{for(var i = 0, len = gdjs.S6Code.GDNewObject3Objects1.length ;i < len;++i) {
    gdjs.S6Code.GDNewObject3Objects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S7", true);
}}

}


}; //End of gdjs.S6Code.eventsList0xb25a8


gdjs.S6Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S6Code.GDNewObjectObjects1.length = 0;
gdjs.S6Code.GDNewObjectObjects2.length = 0;
gdjs.S6Code.GDNewObject2Objects1.length = 0;
gdjs.S6Code.GDNewObject2Objects2.length = 0;
gdjs.S6Code.GDNewObject3Objects1.length = 0;
gdjs.S6Code.GDNewObject3Objects2.length = 0;

gdjs.S6Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S6Code'] = gdjs.S6Code;
