gdjs.S12Code = {};
gdjs.S12Code.GDNewObjectObjects1= [];
gdjs.S12Code.GDNewObjectObjects2= [];
gdjs.S12Code.GDNewObject2Objects1= [];
gdjs.S12Code.GDNewObject2Objects2= [];
gdjs.S12Code.GDNewObject3Objects1= [];
gdjs.S12Code.GDNewObject3Objects2= [];

gdjs.S12Code.conditionTrue_0 = {val:false};
gdjs.S12Code.condition0IsTrue_0 = {val:false};
gdjs.S12Code.condition1IsTrue_0 = {val:false};
gdjs.S12Code.condition2IsTrue_0 = {val:false};


gdjs.S12Code.mapOfGDgdjs_46S12Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S12Code.GDNewObject2Objects1});gdjs.S12Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S12Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S12Code.condition0IsTrue_0.val = false;
gdjs.S12Code.condition1IsTrue_0.val = false;
{
gdjs.S12Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S12Code.condition0IsTrue_0.val ) {
{
gdjs.S12Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S12Code.mapOfGDgdjs_46S12Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S12Code.condition1IsTrue_0.val) {
gdjs.S12Code.GDNewObject3Objects1.createFrom(runtimeScene.getObjects("NewObject3"));
{for(var i = 0, len = gdjs.S12Code.GDNewObject3Objects1.length ;i < len;++i) {
    gdjs.S12Code.GDNewObject3Objects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S13", true);
}}

}


}; //End of gdjs.S12Code.eventsList0xb25a8


gdjs.S12Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S12Code.GDNewObjectObjects1.length = 0;
gdjs.S12Code.GDNewObjectObjects2.length = 0;
gdjs.S12Code.GDNewObject2Objects1.length = 0;
gdjs.S12Code.GDNewObject2Objects2.length = 0;
gdjs.S12Code.GDNewObject3Objects1.length = 0;
gdjs.S12Code.GDNewObject3Objects2.length = 0;

gdjs.S12Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S12Code'] = gdjs.S12Code;
