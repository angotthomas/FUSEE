gdjs.S1Code = {};
gdjs.S1Code.GDNewObjectObjects1= [];
gdjs.S1Code.GDNewObjectObjects2= [];
gdjs.S1Code.GDNewObject2Objects1= [];
gdjs.S1Code.GDNewObject2Objects2= [];

gdjs.S1Code.conditionTrue_0 = {val:false};
gdjs.S1Code.condition0IsTrue_0 = {val:false};
gdjs.S1Code.condition1IsTrue_0 = {val:false};
gdjs.S1Code.condition2IsTrue_0 = {val:false};


gdjs.S1Code.mapOfGDgdjs_46S1Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S1Code.GDNewObject2Objects1});gdjs.S1Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S1Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S1Code.condition0IsTrue_0.val = false;
gdjs.S1Code.condition1IsTrue_0.val = false;
{
gdjs.S1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S1Code.condition0IsTrue_0.val ) {
{
gdjs.S1Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S1Code.mapOfGDgdjs_46S1Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S1Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S2", true);
}}

}


{


{
}

}


}; //End of gdjs.S1Code.eventsList0xb25a8


gdjs.S1Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S1Code.GDNewObjectObjects1.length = 0;
gdjs.S1Code.GDNewObjectObjects2.length = 0;
gdjs.S1Code.GDNewObject2Objects1.length = 0;
gdjs.S1Code.GDNewObject2Objects2.length = 0;

gdjs.S1Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S1Code'] = gdjs.S1Code;
