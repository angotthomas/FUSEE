gdjs.S8Code = {};
gdjs.S8Code.GDNewObjectObjects1= [];
gdjs.S8Code.GDNewObjectObjects2= [];
gdjs.S8Code.GDNewObject2Objects1= [];
gdjs.S8Code.GDNewObject2Objects2= [];
gdjs.S8Code.GDNewObject3Objects1= [];
gdjs.S8Code.GDNewObject3Objects2= [];

gdjs.S8Code.conditionTrue_0 = {val:false};
gdjs.S8Code.condition0IsTrue_0 = {val:false};
gdjs.S8Code.condition1IsTrue_0 = {val:false};
gdjs.S8Code.condition2IsTrue_0 = {val:false};


gdjs.S8Code.mapOfGDgdjs_46S8Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S8Code.GDNewObject2Objects1});gdjs.S8Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S8Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S8Code.condition0IsTrue_0.val = false;
gdjs.S8Code.condition1IsTrue_0.val = false;
{
gdjs.S8Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S8Code.condition0IsTrue_0.val ) {
{
gdjs.S8Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S8Code.mapOfGDgdjs_46S8Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S8Code.condition1IsTrue_0.val) {
gdjs.S8Code.GDNewObject3Objects1.createFrom(runtimeScene.getObjects("NewObject3"));
{for(var i = 0, len = gdjs.S8Code.GDNewObject3Objects1.length ;i < len;++i) {
    gdjs.S8Code.GDNewObject3Objects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S9", true);
}}

}


}; //End of gdjs.S8Code.eventsList0xb25a8


gdjs.S8Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S8Code.GDNewObjectObjects1.length = 0;
gdjs.S8Code.GDNewObjectObjects2.length = 0;
gdjs.S8Code.GDNewObject2Objects1.length = 0;
gdjs.S8Code.GDNewObject2Objects2.length = 0;
gdjs.S8Code.GDNewObject3Objects1.length = 0;
gdjs.S8Code.GDNewObject3Objects2.length = 0;

gdjs.S8Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S8Code'] = gdjs.S8Code;
