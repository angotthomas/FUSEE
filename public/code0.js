gdjs.INTROCode = {};
gdjs.INTROCode.GDTitreObjects1= [];
gdjs.INTROCode.GDTitreObjects2= [];
gdjs.INTROCode.GDclignoObjects1= [];
gdjs.INTROCode.GDclignoObjects2= [];

gdjs.INTROCode.conditionTrue_0 = {val:false};
gdjs.INTROCode.condition0IsTrue_0 = {val:false};
gdjs.INTROCode.condition1IsTrue_0 = {val:false};
gdjs.INTROCode.condition2IsTrue_0 = {val:false};


gdjs.INTROCode.mapOfGDgdjs_46INTROCode_46GDclignoObjects1Objects = Hashtable.newFrom({"cligno": gdjs.INTROCode.GDclignoObjects1});gdjs.INTROCode.mapOfGDgdjs_46INTROCode_46GDTitreObjects1Objects = Hashtable.newFrom({"Titre": gdjs.INTROCode.GDTitreObjects1});gdjs.INTROCode.userFunc0x68a518 = function(runtimeScene) {
alert("Il faut cliquer sur le message en jaune !!!");
};
gdjs.INTROCode.eventsList0x68a370 = function(runtimeScene) {

{


gdjs.INTROCode.userFunc0x68a518(runtimeScene);

}


}; //End of gdjs.INTROCode.eventsList0x68a370
gdjs.INTROCode.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.INTROCode.GDclignoObjects1.createFrom(runtimeScene.getObjects("cligno"));

gdjs.INTROCode.condition0IsTrue_0.val = false;
gdjs.INTROCode.condition1IsTrue_0.val = false;
{
gdjs.INTROCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.INTROCode.condition0IsTrue_0.val ) {
{
gdjs.INTROCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.INTROCode.mapOfGDgdjs_46INTROCode_46GDclignoObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.INTROCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S1", false);
}}

}


{

gdjs.INTROCode.GDTitreObjects1.createFrom(runtimeScene.getObjects("Titre"));

gdjs.INTROCode.condition0IsTrue_0.val = false;
gdjs.INTROCode.condition1IsTrue_0.val = false;
{
gdjs.INTROCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.INTROCode.condition0IsTrue_0.val ) {
{
gdjs.INTROCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.INTROCode.mapOfGDgdjs_46INTROCode_46GDTitreObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.INTROCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.INTROCode.eventsList0x68a370(runtimeScene);} //End of subevents
}

}


{

gdjs.INTROCode.GDclignoObjects1.createFrom(runtimeScene.getObjects("cligno"));

gdjs.INTROCode.condition0IsTrue_0.val = false;
gdjs.INTROCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.INTROCode.GDclignoObjects1.length;i<l;++i) {
    if ( gdjs.INTROCode.GDclignoObjects1[i].timerElapsedTime("timer", 1) ) {
        gdjs.INTROCode.condition0IsTrue_0.val = true;
        gdjs.INTROCode.GDclignoObjects1[k] = gdjs.INTROCode.GDclignoObjects1[i];
        ++k;
    }
}
gdjs.INTROCode.GDclignoObjects1.length = k;}if ( gdjs.INTROCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.INTROCode.GDclignoObjects1.length;i<l;++i) {
    if ( gdjs.INTROCode.GDclignoObjects1[i].isVisible() ) {
        gdjs.INTROCode.condition1IsTrue_0.val = true;
        gdjs.INTROCode.GDclignoObjects1[k] = gdjs.INTROCode.GDclignoObjects1[i];
        ++k;
    }
}
gdjs.INTROCode.GDclignoObjects1.length = k;}}
if (gdjs.INTROCode.condition1IsTrue_0.val) {
/* Reuse gdjs.INTROCode.GDclignoObjects1 */
{for(var i = 0, len = gdjs.INTROCode.GDclignoObjects1.length ;i < len;++i) {
    gdjs.INTROCode.GDclignoObjects1[i].hide();
}
}}

}


{

gdjs.INTROCode.GDclignoObjects1.createFrom(runtimeScene.getObjects("cligno"));

gdjs.INTROCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.INTROCode.GDclignoObjects1.length;i<l;++i) {
    if ( gdjs.INTROCode.GDclignoObjects1[i].timerElapsedTime("timer", 2) ) {
        gdjs.INTROCode.condition0IsTrue_0.val = true;
        gdjs.INTROCode.GDclignoObjects1[k] = gdjs.INTROCode.GDclignoObjects1[i];
        ++k;
    }
}
gdjs.INTROCode.GDclignoObjects1.length = k;}if (gdjs.INTROCode.condition0IsTrue_0.val) {
/* Reuse gdjs.INTROCode.GDclignoObjects1 */
{for(var i = 0, len = gdjs.INTROCode.GDclignoObjects1.length ;i < len;++i) {
    gdjs.INTROCode.GDclignoObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.INTROCode.GDclignoObjects1.length ;i < len;++i) {
    gdjs.INTROCode.GDclignoObjects1[i].resetTimer("timer");
}
}}

}


{


gdjs.INTROCode.condition0IsTrue_0.val = false;
{
gdjs.INTROCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.INTROCode.condition0IsTrue_0.val) {
gdjs.INTROCode.GDclignoObjects1.createFrom(runtimeScene.getObjects("cligno"));
{for(var i = 0, len = gdjs.INTROCode.GDclignoObjects1.length ;i < len;++i) {
    gdjs.INTROCode.GDclignoObjects1[i].resetTimer("timer");
}
}}

}


}; //End of gdjs.INTROCode.eventsList0xb25a8


gdjs.INTROCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.INTROCode.GDTitreObjects1.length = 0;
gdjs.INTROCode.GDTitreObjects2.length = 0;
gdjs.INTROCode.GDclignoObjects1.length = 0;
gdjs.INTROCode.GDclignoObjects2.length = 0;

gdjs.INTROCode.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['INTROCode'] = gdjs.INTROCode;
