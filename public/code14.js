gdjs.S14Code = {};
gdjs.S14Code.GDNewObjectObjects1= [];
gdjs.S14Code.GDNewObjectObjects2= [];
gdjs.S14Code.GDNewObject2Objects1= [];
gdjs.S14Code.GDNewObject2Objects2= [];
gdjs.S14Code.GDNewObject3Objects1= [];
gdjs.S14Code.GDNewObject3Objects2= [];

gdjs.S14Code.conditionTrue_0 = {val:false};
gdjs.S14Code.condition0IsTrue_0 = {val:false};
gdjs.S14Code.condition1IsTrue_0 = {val:false};
gdjs.S14Code.condition2IsTrue_0 = {val:false};


gdjs.S14Code.mapOfGDgdjs_46S14Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S14Code.GDNewObject2Objects1});gdjs.S14Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S14Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S14Code.condition0IsTrue_0.val = false;
gdjs.S14Code.condition1IsTrue_0.val = false;
{
gdjs.S14Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S14Code.condition0IsTrue_0.val ) {
{
gdjs.S14Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S14Code.mapOfGDgdjs_46S14Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S14Code.condition1IsTrue_0.val) {
gdjs.S14Code.GDNewObject3Objects1.createFrom(runtimeScene.getObjects("NewObject3"));
{for(var i = 0, len = gdjs.S14Code.GDNewObject3Objects1.length ;i < len;++i) {
    gdjs.S14Code.GDNewObject3Objects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S15", true);
}}

}


}; //End of gdjs.S14Code.eventsList0xb25a8


gdjs.S14Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S14Code.GDNewObjectObjects1.length = 0;
gdjs.S14Code.GDNewObjectObjects2.length = 0;
gdjs.S14Code.GDNewObject2Objects1.length = 0;
gdjs.S14Code.GDNewObject2Objects2.length = 0;
gdjs.S14Code.GDNewObject3Objects1.length = 0;
gdjs.S14Code.GDNewObject3Objects2.length = 0;

gdjs.S14Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S14Code'] = gdjs.S14Code;
