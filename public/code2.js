gdjs.S2Code = {};
gdjs.S2Code.GDNewObjectObjects1= [];
gdjs.S2Code.GDNewObjectObjects2= [];
gdjs.S2Code.GDNewObject2Objects1= [];
gdjs.S2Code.GDNewObject2Objects2= [];

gdjs.S2Code.conditionTrue_0 = {val:false};
gdjs.S2Code.condition0IsTrue_0 = {val:false};
gdjs.S2Code.condition1IsTrue_0 = {val:false};
gdjs.S2Code.condition2IsTrue_0 = {val:false};


gdjs.S2Code.mapOfGDgdjs_46S2Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S2Code.GDNewObject2Objects1});gdjs.S2Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S2Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S2Code.condition0IsTrue_0.val = false;
gdjs.S2Code.condition1IsTrue_0.val = false;
{
gdjs.S2Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S2Code.condition0IsTrue_0.val ) {
{
gdjs.S2Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S2Code.mapOfGDgdjs_46S2Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S2Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S3", true);
}}

}


}; //End of gdjs.S2Code.eventsList0xb25a8


gdjs.S2Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S2Code.GDNewObjectObjects1.length = 0;
gdjs.S2Code.GDNewObjectObjects2.length = 0;
gdjs.S2Code.GDNewObject2Objects1.length = 0;
gdjs.S2Code.GDNewObject2Objects2.length = 0;

gdjs.S2Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S2Code'] = gdjs.S2Code;
