gdjs.S7Code = {};
gdjs.S7Code.GDNewObjectObjects1= [];
gdjs.S7Code.GDNewObjectObjects2= [];
gdjs.S7Code.GDNewObject2Objects1= [];
gdjs.S7Code.GDNewObject2Objects2= [];
gdjs.S7Code.GDNewObject3Objects1= [];
gdjs.S7Code.GDNewObject3Objects2= [];

gdjs.S7Code.conditionTrue_0 = {val:false};
gdjs.S7Code.condition0IsTrue_0 = {val:false};
gdjs.S7Code.condition1IsTrue_0 = {val:false};
gdjs.S7Code.condition2IsTrue_0 = {val:false};


gdjs.S7Code.mapOfGDgdjs_46S7Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S7Code.GDNewObject2Objects1});gdjs.S7Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S7Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S7Code.condition0IsTrue_0.val = false;
gdjs.S7Code.condition1IsTrue_0.val = false;
{
gdjs.S7Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S7Code.condition0IsTrue_0.val ) {
{
gdjs.S7Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S7Code.mapOfGDgdjs_46S7Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S7Code.condition1IsTrue_0.val) {
gdjs.S7Code.GDNewObject3Objects1.createFrom(runtimeScene.getObjects("NewObject3"));
{for(var i = 0, len = gdjs.S7Code.GDNewObject3Objects1.length ;i < len;++i) {
    gdjs.S7Code.GDNewObject3Objects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S8", true);
}}

}


}; //End of gdjs.S7Code.eventsList0xb25a8


gdjs.S7Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S7Code.GDNewObjectObjects1.length = 0;
gdjs.S7Code.GDNewObjectObjects2.length = 0;
gdjs.S7Code.GDNewObject2Objects1.length = 0;
gdjs.S7Code.GDNewObject2Objects2.length = 0;
gdjs.S7Code.GDNewObject3Objects1.length = 0;
gdjs.S7Code.GDNewObject3Objects2.length = 0;

gdjs.S7Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S7Code'] = gdjs.S7Code;
