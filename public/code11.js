gdjs.S11Code = {};
gdjs.S11Code.GDNewObjectObjects1= [];
gdjs.S11Code.GDNewObjectObjects2= [];
gdjs.S11Code.GDNewObject2Objects1= [];
gdjs.S11Code.GDNewObject2Objects2= [];
gdjs.S11Code.GDNewObject3Objects1= [];
gdjs.S11Code.GDNewObject3Objects2= [];

gdjs.S11Code.conditionTrue_0 = {val:false};
gdjs.S11Code.condition0IsTrue_0 = {val:false};
gdjs.S11Code.condition1IsTrue_0 = {val:false};
gdjs.S11Code.condition2IsTrue_0 = {val:false};


gdjs.S11Code.mapOfGDgdjs_46S11Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S11Code.GDNewObject2Objects1});gdjs.S11Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S11Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S11Code.condition0IsTrue_0.val = false;
gdjs.S11Code.condition1IsTrue_0.val = false;
{
gdjs.S11Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S11Code.condition0IsTrue_0.val ) {
{
gdjs.S11Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S11Code.mapOfGDgdjs_46S11Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S11Code.condition1IsTrue_0.val) {
gdjs.S11Code.GDNewObject3Objects1.createFrom(runtimeScene.getObjects("NewObject3"));
{for(var i = 0, len = gdjs.S11Code.GDNewObject3Objects1.length ;i < len;++i) {
    gdjs.S11Code.GDNewObject3Objects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S12", true);
}}

}


}; //End of gdjs.S11Code.eventsList0xb25a8


gdjs.S11Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S11Code.GDNewObjectObjects1.length = 0;
gdjs.S11Code.GDNewObjectObjects2.length = 0;
gdjs.S11Code.GDNewObject2Objects1.length = 0;
gdjs.S11Code.GDNewObject2Objects2.length = 0;
gdjs.S11Code.GDNewObject3Objects1.length = 0;
gdjs.S11Code.GDNewObject3Objects2.length = 0;

gdjs.S11Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S11Code'] = gdjs.S11Code;
