gdjs.S13Code = {};
gdjs.S13Code.GDNewObjectObjects1= [];
gdjs.S13Code.GDNewObjectObjects2= [];
gdjs.S13Code.GDNewObject2Objects1= [];
gdjs.S13Code.GDNewObject2Objects2= [];
gdjs.S13Code.GDNewObject3Objects1= [];
gdjs.S13Code.GDNewObject3Objects2= [];

gdjs.S13Code.conditionTrue_0 = {val:false};
gdjs.S13Code.condition0IsTrue_0 = {val:false};
gdjs.S13Code.condition1IsTrue_0 = {val:false};
gdjs.S13Code.condition2IsTrue_0 = {val:false};


gdjs.S13Code.mapOfGDgdjs_46S13Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S13Code.GDNewObject2Objects1});gdjs.S13Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S13Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S13Code.condition0IsTrue_0.val = false;
gdjs.S13Code.condition1IsTrue_0.val = false;
{
gdjs.S13Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S13Code.condition0IsTrue_0.val ) {
{
gdjs.S13Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S13Code.mapOfGDgdjs_46S13Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S13Code.condition1IsTrue_0.val) {
gdjs.S13Code.GDNewObject3Objects1.createFrom(runtimeScene.getObjects("NewObject3"));
{for(var i = 0, len = gdjs.S13Code.GDNewObject3Objects1.length ;i < len;++i) {
    gdjs.S13Code.GDNewObject3Objects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S14", true);
}}

}


}; //End of gdjs.S13Code.eventsList0xb25a8


gdjs.S13Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S13Code.GDNewObjectObjects1.length = 0;
gdjs.S13Code.GDNewObjectObjects2.length = 0;
gdjs.S13Code.GDNewObject2Objects1.length = 0;
gdjs.S13Code.GDNewObject2Objects2.length = 0;
gdjs.S13Code.GDNewObject3Objects1.length = 0;
gdjs.S13Code.GDNewObject3Objects2.length = 0;

gdjs.S13Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S13Code'] = gdjs.S13Code;
