gdjs.S3Code = {};
gdjs.S3Code.GDNewObjectObjects1= [];
gdjs.S3Code.GDNewObjectObjects2= [];
gdjs.S3Code.GDNewObject2Objects1= [];
gdjs.S3Code.GDNewObject2Objects2= [];

gdjs.S3Code.conditionTrue_0 = {val:false};
gdjs.S3Code.condition0IsTrue_0 = {val:false};
gdjs.S3Code.condition1IsTrue_0 = {val:false};
gdjs.S3Code.condition2IsTrue_0 = {val:false};


gdjs.S3Code.mapOfGDgdjs_46S3Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S3Code.GDNewObject2Objects1});gdjs.S3Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S3Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S3Code.condition0IsTrue_0.val = false;
gdjs.S3Code.condition1IsTrue_0.val = false;
{
gdjs.S3Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S3Code.condition0IsTrue_0.val ) {
{
gdjs.S3Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S3Code.mapOfGDgdjs_46S3Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S3Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S4", true);
}}

}


}; //End of gdjs.S3Code.eventsList0xb25a8


gdjs.S3Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S3Code.GDNewObjectObjects1.length = 0;
gdjs.S3Code.GDNewObjectObjects2.length = 0;
gdjs.S3Code.GDNewObject2Objects1.length = 0;
gdjs.S3Code.GDNewObject2Objects2.length = 0;

gdjs.S3Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S3Code'] = gdjs.S3Code;
