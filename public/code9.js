gdjs.S9Code = {};
gdjs.S9Code.GDNewObjectObjects1= [];
gdjs.S9Code.GDNewObjectObjects2= [];
gdjs.S9Code.GDNewObject2Objects1= [];
gdjs.S9Code.GDNewObject2Objects2= [];
gdjs.S9Code.GDNewObject3Objects1= [];
gdjs.S9Code.GDNewObject3Objects2= [];

gdjs.S9Code.conditionTrue_0 = {val:false};
gdjs.S9Code.condition0IsTrue_0 = {val:false};
gdjs.S9Code.condition1IsTrue_0 = {val:false};
gdjs.S9Code.condition2IsTrue_0 = {val:false};


gdjs.S9Code.mapOfGDgdjs_46S9Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S9Code.GDNewObject2Objects1});gdjs.S9Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S9Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S9Code.condition0IsTrue_0.val = false;
gdjs.S9Code.condition1IsTrue_0.val = false;
{
gdjs.S9Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S9Code.condition0IsTrue_0.val ) {
{
gdjs.S9Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S9Code.mapOfGDgdjs_46S9Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S9Code.condition1IsTrue_0.val) {
gdjs.S9Code.GDNewObject3Objects1.createFrom(runtimeScene.getObjects("NewObject3"));
{for(var i = 0, len = gdjs.S9Code.GDNewObject3Objects1.length ;i < len;++i) {
    gdjs.S9Code.GDNewObject3Objects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S10", true);
}}

}


}; //End of gdjs.S9Code.eventsList0xb25a8


gdjs.S9Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S9Code.GDNewObjectObjects1.length = 0;
gdjs.S9Code.GDNewObjectObjects2.length = 0;
gdjs.S9Code.GDNewObject2Objects1.length = 0;
gdjs.S9Code.GDNewObject2Objects2.length = 0;
gdjs.S9Code.GDNewObject3Objects1.length = 0;
gdjs.S9Code.GDNewObject3Objects2.length = 0;

gdjs.S9Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S9Code'] = gdjs.S9Code;
