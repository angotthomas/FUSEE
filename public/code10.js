gdjs.S10Code = {};
gdjs.S10Code.GDNewObjectObjects1= [];
gdjs.S10Code.GDNewObjectObjects2= [];
gdjs.S10Code.GDNewObject2Objects1= [];
gdjs.S10Code.GDNewObject2Objects2= [];
gdjs.S10Code.GDNewObject3Objects1= [];
gdjs.S10Code.GDNewObject3Objects2= [];

gdjs.S10Code.conditionTrue_0 = {val:false};
gdjs.S10Code.condition0IsTrue_0 = {val:false};
gdjs.S10Code.condition1IsTrue_0 = {val:false};
gdjs.S10Code.condition2IsTrue_0 = {val:false};


gdjs.S10Code.mapOfGDgdjs_46S10Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S10Code.GDNewObject2Objects1});gdjs.S10Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S10Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S10Code.condition0IsTrue_0.val = false;
gdjs.S10Code.condition1IsTrue_0.val = false;
{
gdjs.S10Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S10Code.condition0IsTrue_0.val ) {
{
gdjs.S10Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S10Code.mapOfGDgdjs_46S10Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S10Code.condition1IsTrue_0.val) {
gdjs.S10Code.GDNewObject3Objects1.createFrom(runtimeScene.getObjects("NewObject3"));
{for(var i = 0, len = gdjs.S10Code.GDNewObject3Objects1.length ;i < len;++i) {
    gdjs.S10Code.GDNewObject3Objects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S11", true);
}}

}


}; //End of gdjs.S10Code.eventsList0xb25a8


gdjs.S10Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S10Code.GDNewObjectObjects1.length = 0;
gdjs.S10Code.GDNewObjectObjects2.length = 0;
gdjs.S10Code.GDNewObject2Objects1.length = 0;
gdjs.S10Code.GDNewObject2Objects2.length = 0;
gdjs.S10Code.GDNewObject3Objects1.length = 0;
gdjs.S10Code.GDNewObject3Objects2.length = 0;

gdjs.S10Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S10Code'] = gdjs.S10Code;
