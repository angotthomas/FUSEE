gdjs.S5Code = {};
gdjs.S5Code.GDNewObjectObjects1= [];
gdjs.S5Code.GDNewObjectObjects2= [];
gdjs.S5Code.GDTransObjects1= [];
gdjs.S5Code.GDTransObjects2= [];
gdjs.S5Code.GDNewObject2Objects1= [];
gdjs.S5Code.GDNewObject2Objects2= [];
gdjs.S5Code.GDmenuObjects1= [];
gdjs.S5Code.GDmenuObjects2= [];

gdjs.S5Code.conditionTrue_0 = {val:false};
gdjs.S5Code.condition0IsTrue_0 = {val:false};
gdjs.S5Code.condition1IsTrue_0 = {val:false};
gdjs.S5Code.condition2IsTrue_0 = {val:false};


gdjs.S5Code.mapOfGDgdjs_46S5Code_46GDTransObjects1Objects = Hashtable.newFrom({"Trans": gdjs.S5Code.GDTransObjects1});gdjs.S5Code.mapOfGDgdjs_46S5Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S5Code.GDNewObject2Objects1});gdjs.S5Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S5Code.GDTransObjects1.createFrom(runtimeScene.getObjects("Trans"));

gdjs.S5Code.condition0IsTrue_0.val = false;
gdjs.S5Code.condition1IsTrue_0.val = false;
{
gdjs.S5Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S5Code.condition0IsTrue_0.val ) {
{
gdjs.S5Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S5Code.mapOfGDgdjs_46S5Code_46GDTransObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.S5Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S6", true);
}}

}


{

gdjs.S5Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S5Code.condition0IsTrue_0.val = false;
gdjs.S5Code.condition1IsTrue_0.val = false;
{
gdjs.S5Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Right");
}if ( gdjs.S5Code.condition0IsTrue_0.val ) {
{
gdjs.S5Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S5Code.mapOfGDgdjs_46S5Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S5Code.condition1IsTrue_0.val) {
gdjs.S5Code.GDTransObjects1.createFrom(runtimeScene.getObjects("Trans"));
gdjs.S5Code.GDmenuObjects1.createFrom(runtimeScene.getObjects("menu"));
{for(var i = 0, len = gdjs.S5Code.GDmenuObjects1.length ;i < len;++i) {
    gdjs.S5Code.GDmenuObjects1[i].hide(false);
}
}{for(var i = 0, len = gdjs.S5Code.GDTransObjects1.length ;i < len;++i) {
    gdjs.S5Code.GDTransObjects1[i].hide(false);
}
}}

}


{


gdjs.S5Code.condition0IsTrue_0.val = false;
{
gdjs.S5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.S5Code.condition0IsTrue_0.val) {
gdjs.S5Code.GDTransObjects1.createFrom(runtimeScene.getObjects("Trans"));
gdjs.S5Code.GDmenuObjects1.createFrom(runtimeScene.getObjects("menu"));
{for(var i = 0, len = gdjs.S5Code.GDmenuObjects1.length ;i < len;++i) {
    gdjs.S5Code.GDmenuObjects1[i].hide();
}
}{for(var i = 0, len = gdjs.S5Code.GDTransObjects1.length ;i < len;++i) {
    gdjs.S5Code.GDTransObjects1[i].hide();
}
}}

}


}; //End of gdjs.S5Code.eventsList0xb25a8


gdjs.S5Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S5Code.GDNewObjectObjects1.length = 0;
gdjs.S5Code.GDNewObjectObjects2.length = 0;
gdjs.S5Code.GDTransObjects1.length = 0;
gdjs.S5Code.GDTransObjects2.length = 0;
gdjs.S5Code.GDNewObject2Objects1.length = 0;
gdjs.S5Code.GDNewObject2Objects2.length = 0;
gdjs.S5Code.GDmenuObjects1.length = 0;
gdjs.S5Code.GDmenuObjects2.length = 0;

gdjs.S5Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S5Code'] = gdjs.S5Code;
