gdjs.S4Code = {};
gdjs.S4Code.GDNewObjectObjects1= [];
gdjs.S4Code.GDNewObjectObjects2= [];
gdjs.S4Code.GDNewObject2Objects1= [];
gdjs.S4Code.GDNewObject2Objects2= [];
gdjs.S4Code.GDNewObject3Objects1= [];
gdjs.S4Code.GDNewObject3Objects2= [];

gdjs.S4Code.conditionTrue_0 = {val:false};
gdjs.S4Code.condition0IsTrue_0 = {val:false};
gdjs.S4Code.condition1IsTrue_0 = {val:false};
gdjs.S4Code.condition2IsTrue_0 = {val:false};


gdjs.S4Code.mapOfGDgdjs_46S4Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.S4Code.GDNewObject2Objects1});gdjs.S4Code.eventsList0xb25a8 = function(runtimeScene) {

{

gdjs.S4Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.S4Code.condition0IsTrue_0.val = false;
gdjs.S4Code.condition1IsTrue_0.val = false;
{
gdjs.S4Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.S4Code.condition0IsTrue_0.val ) {
{
gdjs.S4Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.S4Code.mapOfGDgdjs_46S4Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.S4Code.condition1IsTrue_0.val) {
gdjs.S4Code.GDNewObject3Objects1.createFrom(runtimeScene.getObjects("NewObject3"));
{for(var i = 0, len = gdjs.S4Code.GDNewObject3Objects1.length ;i < len;++i) {
    gdjs.S4Code.GDNewObject3Objects1[i].hide();
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "S5", true);
}}

}


}; //End of gdjs.S4Code.eventsList0xb25a8


gdjs.S4Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();
gdjs.S4Code.GDNewObjectObjects1.length = 0;
gdjs.S4Code.GDNewObjectObjects2.length = 0;
gdjs.S4Code.GDNewObject2Objects1.length = 0;
gdjs.S4Code.GDNewObject2Objects2.length = 0;
gdjs.S4Code.GDNewObject3Objects1.length = 0;
gdjs.S4Code.GDNewObject3Objects2.length = 0;

gdjs.S4Code.eventsList0xb25a8(runtimeScene);
return;
}
gdjs['S4Code'] = gdjs.S4Code;
